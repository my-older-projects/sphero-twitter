# sphero-twitter [![NPM version][npm-image]][npm-url]
> Control your smart Robots with twitter.

## Installation

```sh
$ npm install ollie-twitter
```

You need to get your Twitter API keys. ([Guide to get them](http://stackoverflow.com/a/12335636/3873452))
After you get all the 4 keys, copy-n-paste them in ```twitter.credit``` file (create it if it doesn't exist) in JSON like this:
```js
{
	"conKey": "",
	"conSec": "", 
	"accKey": "", 
	"accSec": ""
 }
```

If you are controlling an Ollie or a BB-8, and if you want to use without ```sudo```, then follow [this link.](https://github.com/sandeepmistry/noble#running-without-rootsudo)

You can change which hashtag sphero is listening by passing ´´´.start(str)´´´ method of sphero-twitter a different hashtag in app.js file.


## Usage

```sh
$ node app.js
```

##Syntax

The language I used in parsing tweets is [tiLang](https://github.com/omeryagmurlu/tiLang), look at there for syntax.


####Built-in Functions

You can use every function defined [here](https://github.com/orbotix/sphero.js/blob/master/lib/devices/sphero.js), [here](https://github.com/orbotix/sphero.js/blob/master/lib/devices/custom.js) and [here](https://github.com/orbotix/sphero.js/blob/master/lib/devices/core.js).

In addition to them there are shortcut functions for most-used functions (Twitter 140 char limit) and some helper functions.

######Helper Functions

* ```connect|UUID```
Connects to Ollie/Sphero/BB-8. If already connected, does nothing.

* ```delay|seconds```
Waits before running next command.

######Shortcuts

* ```cnct|UUID``` => ```connect|UUID```
* ```dly|seconds``` => ```delay|seconds```
* ```c|red``` => ```color|red```
* ```r|255|0``` => ```roll|255|0```
* ```sp|lt|255``` => (Spin, Doesn't exist, this is a helper for ```setRawMotors```)
* ```rwM|lmode|rmode|lpower|rpower``` _*=/=>*_ ```setRawMotors|{lmode:lmode,rmode:rmode,lpower:lpower,rpower:rpower}``` (Objects cannot be parsed yet remember?)
* ```bL|255``` => ```setBackLed|255```
* ```stp``` => ```stop```

## License

GPL © [omeryagmurlu](https://omeryagmurlu.github.com)


[npm-image]: https://badge.fury.io/js/sphero-twitter.svg
[npm-url]: https://npmjs.org/package/sphero-twitter
[travis-image]: https://travis-ci.org/omeryagmurlu/sphero-twitter.svg?branch=master
[travis-url]: https://travis-ci.org/omeryagmurlu/sphero-twitter
[daviddm-image]: https://david-dm.org/omeryagmurlu/sphero-twitter.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/omeryagmurlu/sphero-twitter
