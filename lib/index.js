'use strict'
const Twitter = require('./twitter');
const tiLang = require('ti-lang');
const pastebin = require('./pastebin').pastebin;
const Ollie = require('sphero');

var App = module.exports = function (auth) {
	this.twitter = new Twitter(auth);

	this.ollieConnected = false;	
	let dat = this;
	let options = {
		global: [{}],
		methods: {
			c: (color, cb) => this.ollie.color(color, cb),
			rCo: (cb) => this.ollie.randomColor(cb),
			r: (_1, _2, _3, cb) => this.ollie.roll(_1, _2, _3, cb),
			sp: function(direction, speed, speed2, callback) { //speed 2 is optional // spin
				if (typeof speed2 === "function") {
					var callback = speed2;
					var speed2 = speed;
				}
				var rightMap = {
					lt: 0x02,
					rt: 0x01
				}
				var leftMap = {
					lt: 0x01,
					rt: 0x02
				}

				this.ollie.setRawMotors({
					lmode: leftMap[direction],
					rmode: rightMap[direction],
					lpower: speed,
					rpower: speed2
				}, callback);
			}.bind(this),
			rwM: function(lmode, rmode, lpower, rpower, callback) { //setRawMotors
				var dir = {
					o: 0x00,
					f: 0x01,
					r: 0x02,
					b: 0x03,
					i: 0x04
				}

				var com = {
					lmode: dir[lmode],
					rmode: dir[rmode],
					lpower: lpower,
					rpower: rpower
				}

				this.ollie.setRawMotors(com, callback)
			}.bind(this),
			bL: (_1, cb) => this.ollie.setBackLed(_1, cb),
			rBL: (cb) => this.ollie.setBackLed(Math.floor(Math.random() * (255 - 0 + 1)) + 0, cb),
			stp: (cb) => this.ollie.stop(cb),
			p: (str, cb) => {
				console.log('Ollie says: ', str);
				cb();
			},
			slp: (cb) => {
				return this.ollie.sleep(0, 0, 0, () => {
					this.ollieConnected = false;
					cb();
				});
			} 
		}
	};
	options.methods.cnct = options.methods.connect = (UUID, cb) => {
		if (!this.ollieConnected) {
			this.ollie = new Ollie(UUID);
			this.ollie.connect(() => {
				this.ollieConnected = true;
				cb();
			})
		} else {
			cb();
		}
	}

	this.lang = new tiLang(options);
}

App.prototype.start = function(str) {  // str is the hashtag to listen
	this.twitter.listen(str, function(tweet) {
		if (tweet.text.includes("#pastebin")) {
			pastebin(this, tweet.text)
		} else {
			this._runText(tweet.text)
		}
	}.bind(this));

	return this;
};

App.prototype._runText = function(str) {
	this.lang.run(str);
	return this;
};

App.prototype.offlineStart = function() {  // str is the hashtag to listen
	var watchr = require('watchr');
	var fs = require('fs');
	var that = this;

	watchr.watch({
		path:'offline.ti',
		listener(type,path,curr,prev) {
			that._runText(fs.readFileSync(path, "utf8"));
		},
	});

	return this;
};