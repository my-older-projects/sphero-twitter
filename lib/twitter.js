'use strict';
var twitter = require('twitter');

var Twitter = module.exports = function (auth) {
	this.client = new twitter({
		consumer_key: auth.conKey,
		consumer_secret: auth.conSec,
		access_token_key: auth.accKey,
		access_token_secret: auth.accSec
	});
};

Twitter.prototype.listen = function(toString, callback) {
	this.client.stream('statuses/filter', {track: toString}, function(stream) {
		stream.on('data', function(tweet) {
			callback(tweet);
		});

		stream.on('error', function(error) {
			console.log(error)
			throw error;
		});
	});
};