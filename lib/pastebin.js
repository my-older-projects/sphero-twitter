const http = require('http');

exports.pastebin = function(that, text) {
	var link = text.replace(/#[A-Za-z0-9_]*/g, '').replace(/\s*/g, '');

	http.get(`http://pastebin.com/raw/` + link, function(response) {
		response.on('data', function(body) {

			that._runText(body.toString());

		});
	}).on('error', function(e) {
		console.log("Http GET Errored: " + e.message);
	});
}