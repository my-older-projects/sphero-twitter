'use strict'
var OllieTwitter = require('./lib');

/* Enter your twitter **API** creditentals in twitter.credit file like this
 *	{
 *		"conKey": "",
 *		"conSec": "", 
 *		"accKey": "", 
 *		"accSec": ""
 *	}	
 */

var ollieServices = new OllieTwitter(JSON.parse((require('fs')).readFileSync('twitter.credit')));

ollieServices.start('#cO0');
// ollieServices.offlineStart();